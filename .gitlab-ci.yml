stages:
  - build-containers
  - test
  - build
  - deploy
  - verify
  - release

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  LC_ALL: "C.UTF-8"
  LANG: "C.UTF-8"

cache:
  paths:
    - .venv/

image: registry.gitlab.com/sublime-music/sublime-music/python-build:latest

lint:
  stage: test
  before_script:
    - ./cicd/install-project-deps.sh
  script:
    - pipenv run python setup.py check -mrs
    - pipenv run black --check .
    - pipenv run flake8
    - pipenv run mypy sublime tests/**/*.py
    - pipenv run cicd/custom_style_check.py

test:
  stage: test
  before_script:
    - ./cicd/install-project-deps.sh
    - ./cicd/start-dbus.sh
    - apt install xvfb
    - Xvfb :119 -screen 0 1024x768x16 &
    - export DISPLAY=:119
  script:
    - pipenv run ./cicd/pytest.sh
  artifacts:
    paths:
      - htmlcov

build:
  stage: build
  before_script:
    - ./cicd/install-project-deps.sh
  script:
    - pipenv run python setup.py sdist
  artifacts:
    paths:
      - dist/*
    expire_in: 7 days

build_flatpak:
  image: registry.gitlab.com/sublime-music/sublime-music/flatpak-build:latest
  allow_failure: true
  stage: build
  script:
    - cd flatpak
    - ./flatpak_build.sh
  artifacts:
    paths:
      - flatpak/sublime-music.flatpak

# Deploy documenation and code coverage to GitLab Pages
pages:
  only:
    - master
  stage: deploy
  before_script:
    - ./cicd/install-project-deps.sh
    - ./cicd/start-dbus.sh
  script:
    - ./cicd/pages-build.sh
  artifacts:
    paths:
      - public

deploy_pypi:
  image: python:3.8-alpine
  stage: deploy
  cache: {}
  only:
    variables:
      # Only do a deploy if it's a version tag.
      - $CI_COMMIT_TAG =~ /^v.*/
  before_script:
    - apk add gcc musl-dev libffi-dev openssl-dev
    - pip install twine
    - './cicd/create-pypirc.sh'
  script:
    # Upload to the test PyPi instance, then upload to the real one.
    - twine upload -r pypi_test dist/*
    - twine upload dist/*
  after_script:
    - echo "" > ~/.pypirc && rm -f ~/.pypirc

verify_deploy:
  stage: verify
  cache: {}
  only:
    variables:
      # Only verify the deploy if it's a version tag.
      - $CI_COMMIT_TAG =~ /^v.*/
  script:
    - pip3 install sublime-music

publish_release:
  stage: release
  only:
    variables:
      # Only verify the deploy if it's a version tag.
      - $CI_COMMIT_TAG =~ /^v.*/
  before_script:
    - ./cicd/install-project-deps.sh
  script:
    - pipenv run ./cicd/publish-gitlab-release.sh

# Scheduled Jobs
# =============================================================================

# Create the Docker image for Flatpak builds.
flatpak_docker:
  image: docker:1.13
  stage: build-containers
  only:
    variables:
      - $REBUILD_FLATPAK_DOCKER == "True"
  services:
    - docker:dind
  script:
    - docker version
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE/flatpak-build:latest ./cicd/flatpak-build
    - docker push $CI_REGISTRY_IMAGE/flatpak-build

# Create the Docker image for Python testing/builds.
python_docker:
  image: docker:1.13
  stage: build-containers
  only:
    variables:
      - $REBUILD_PYTHON_DOCKER == "True"
  services:
    - docker:dind
  script:
    - docker version
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE/python-build:latest ./cicd/python-build
    - docker push $CI_REGISTRY_IMAGE/python-build:latest
